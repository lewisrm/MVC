<?php

/**
 * Base Model
 *
 * find all
 * $users = $model->all()
 * ->fetchAll(); //->fetch();
 * /

/*
 *fin by pk
 * $users = $model->find(1)->fetchAll(); //->fetch();
 */

/*
 * find by where
 * $users = $model->findBy('login like "%xp%"')->fetch_all(); //->fetch();
 * $users = $model->findBy('id_perfil = 6')->fetch_all(); //->fetch();
 */

/*
 * save
 * $model->id_personal = '123456789qwertyuio';
 * $model->login = 'test';
 * $model->password = 'test';
 * $model->insert();
 */

/*
 * update
 * $model->find(7);
 * $model->id_personal = 'USHUtest';
 * $model->login = "UHU";
 * $model->iniciales='UHU';
 * $model->update();
 */

abstract class ModelBase
{
    public $db;
    protected $table;
    protected $fields;
    protected $protected_fields;
    protected $field_value = array();
    protected $pk;
    protected $resultSet;

    public function __construct()
    {
        $this->db = SMYSQLI::singleton();
        $this->db->set_charset("utf8");

        foreach ($this->fields as $field) {
            $this->{$field} = null;
        }
    }

    public function all()
    {
        $query = 'select * from ' . $this->table;
        $this->resultSet = $this->db->query($query);
        if ($this->db->error) {
            throw new Exception('Error [(' . $this->db->errno . '): ' . $this->db->error . ']');
        }
        return $this;
    }

    public function find($pk=null)
    {
        $query = 'select * from :table where :attribute = :value';
        $query = str_replace(array(':table', ':attribute', ':value'), array($this->table, $this->pk, $pk), $query);
        $this->resultSet = $this->db->query($query);

        if (is_null($pk)) {
            throw new Exception('Se requiere un id para la búsqueda.');
        }

        if ($this->db->error) {
            throw new Exception('Error [(' . $this->db->errno . '): ' . $this->db->error . ']');
        }
        $this->fillModel();

        if (!$this->resultSet->num_rows) {
            throw new Exception('No se encontraron resultados.');
        }
        return $this;
    }

    /**
     * not use find_by() for make deletes and updates use fin() instead
     * @param $criteria
     * @return $this
     * @throws Exception
     */
    public function findBy($criteria)
    {
        $pk = $this->pk;
        $this->$pk = null;

        $query = 'select * from :table where :criteria';
        $query = str_replace(array(':table', ':criteria'), array($this->table, $criteria), $query);
        $this->resultSet = $this->db->query($query);

        if ($this->db->error) {
            throw new Exception('Error [(' . $this->db->errno . '): ' . $this->db->error . ']');
        }
        return $this;
    }

    public function fetchAll()
    {
        $data = array();

        while ($row = $this->resultSet->fetch_assoc()) {
            $data[] = $row;
        }

        return $data;
    }

    public function fetch()
    {
        return $this->resultSet->fetch_assoc();
    }

    public function lastInsertId()
    {
        return $this->db->insert_id;
    }

    public function insert()
    {
        $query = 'insert into :tabla (:atributos) values(:valores)';
        $attributes = '';
        $valores = '';

        foreach ($this->fields as $key => $field) {
            $attributes .= $field . ',';
        }

        $attributes = rtrim($attributes, ",");

        foreach ($this->fields as $key => $field) {
            $valores .= '"' . $this->$field . '",';
        }

        $valores = rtrim($valores, ",");

        $query = str_replace(array(':tabla', ':atributos', ':valores'), array($this->table, $attributes, $valores), $query);
        $this->db->query($query);

        if ($this->db->error) {
            throw new Exception('Error [(' . $this->db->errno . '): ' . $this->db->error . ']');
        }
    }

    /*
     * for update use the find() method:
     * $model = new ModelData();
     * $model->find(1);
     * $model->attribute = 'something';
     * $model->update;
     */
    public function update()
    {
        $pk = $this->pk;

        if (is_null($this->$pk)) {
            throw new Exception('Se requiere un id para actualizar.');
        }

        $query = 'UPDATE :table SET :items WHERE :pk = :value limit 1';

        $attributes = '';
        $valores = '';
        $items = '';

        foreach ($this->fields as $field) {
            $items .= $field . '= "' . $this->{$field}  . '",';
        }

        $items = rtrim($items,",");
        $pk_value = $this->pk;


        $query = str_replace(array(':table', ':items', ':pk', ':value'), array($this->table, $items, $this->pk, $this->$pk_value), $query);
        $this->db->query($query);

        if ($this->db->error) {
            throw new Exception('Error [(' . $this->db->errno . '): ' . $this->db->error . ']');
        }
    }

    /*
     * for delee use the find() method:
     * $model = new ModelData();
     * $model->find(1);
     * $model->delete();
     */
    public function delete()
    {
        $pk = $this->pk;

        if (is_null($this->$pk)) {
            throw new Exception('Se requiere un id para eliminar.');
        }

        $pk_value = $this->pk;
        $query = "delete from :table where :pk = :value limit 1";
        $query = str_replace(array(':table', ':pk', ':value'), array($this->table, $this->pk, $this->$pk_value), $query);

        $this->db->query($query);

        if ($this->db->error) {
            throw new Exception('Error [(' . $this->db->errno . '): ' . $this->db->error . ']');
        }
    }

    protected function fillModel()
    {
        $resultSet = $this->fetch();

        foreach ($this->fields as $field) {
            $this->{$field} = $resultSet[$field];
        }

        foreach ($this->protected_fields as $field) {
            $this->{$field} = $resultSet[$field];
        }
    }
}