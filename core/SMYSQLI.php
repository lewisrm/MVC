<?php

/**
 * Pre Compiled SQL Query Object
 */
class SMYSQLI extends mysqli
{
    private static $instance = null;

    public function __construct()
    {
        $config = Config::singleton();
        parent::__construct($config->get('dbhost'), $config->get('dbuser'), $config->get('dbpass'), $config->get('dbname'));

        if (mysqli_connect_error()) {
            $message = 'Error en la conexión a la BD [(' . mysqli_connect_errno() . '): ' . mysqli_connect_error() . ']';
            die($message);
        }
    }

    public static function singleton()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}