<?php

/**
 * Created by PhpStorm.
 * User: levi
 * Date: 28/11/2015
 * Time: 09:22 PM
 */
class Autoloader
{
    private $dirs = array();

    public function __construct()
    {
        spl_autoload_register(array($this, "autoload"));
    }

    public function autoload($className)
    {

        $this->register();
        $this->loadClass($className);

    }

    private function register()
    {
        $config = Config::singleton();
        $this->dirs = array(
            "controllers" => $_SERVER['DOCUMENT_ROOT'] . $config->get('project_path') . "/smvc/controllers/",
            "models" => $_SERVER['DOCUMENT_ROOT'] . $config->get('project_path') . "/smvc/models/",
            "core" => $_SERVER['DOCUMENT_ROOT'] . $config->get('project_path') . "/smvc/core/"
        );
    }

    private function loadClass($className)
    {
        $fileExists = false;
        $file = "";

        foreach ($this->dirs as $dir) {
            $file = "{$dir}{$className}.php";
            if (file_exists($file)) {
                $fileExists = true;
                break;
            }

        }


        if ($fileExists) {
            include_once($file);
        } else {
            echo("La clase {$className} no fue encontrado.$file<br/>");

        }
    }
}