<?php

/**
 * Base Controller
 */
abstract class ControllerBase
{

    protected $view;
    public function __construct()
    {
        $this->view = new View();
    }
}