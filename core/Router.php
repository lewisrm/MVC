<?php

/**
 * Created by PhpStorm.
 * User: levi
 * Date: 14/03/2017
 * Time: 04:28 PM
 */
class Router
{
    private $routes;

    public function __construct($routes)
    {
        $this->addRoutes($routes);
    }

    public function addRoute($route, $controller, $action)
    {
        $this->routes[] = new Route($route, $controller, $action);
        return $this;
    }

    public function addRoutes($routes)
    {
        foreach ($routes as $route => $controller) {
            $controlleraction = explode('@', $controller);
            $this->addRoute($route, $controlleraction[0], $controlleraction[1]);
        }
        return $this;
    }

    public function router($request)
    {
        foreach ($this->routes as $route) {
            if ($route->match($request)) {
                return $route;
            }
        }
    }
}