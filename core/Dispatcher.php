<?php

/**
 * Created by PhpStorm.
 * User: levi
 * Date: 14/03/2017
 * Time: 04:36 PM
 */
class Dispatcher
{
    public function dispatch(Route $route, Request $request)
    {
        $controller = $route->createController();
        $action = $route->getAction();
        call_user_func_array(array($controller, $action), array($request));
    }
}