<?php

/**
 * Created by PhpStorm.
 * User: levi
 * Date: 14/03/2017
 * Time: 01:18 PM
 */
class Route
{
    private $route;
    private $controller;

    public function __construct($route, $controller, $action)
    {
        $this->route = $route;
        $this->controller = $controller;
        $this->action = $action;
    }

    public function match(Request $request)
    {
        return $this->route === $request->getParam('r');
    }

    public function createController()
    {
        return new $this->controller();
    }

    public function getAction()
    {
        return $this->action;
    }
}