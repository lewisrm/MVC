<?php

/**
 * Created by PhpStorm.
 * User: levi
 * Date: 14/03/2017
 * Time: 08:45 AM
 */
class Request
{
    private $uri;
    private $params;

    public function __construct()
    {
        $this->uri = $_SERVER['QUERY_STRING'];
        parse_str($this->uri, $this->params);
    }

    public function getParam($key)
    {
        return $this->params[$key];
    }

    public function getParams()
    {
        return $this->params;
    }
}