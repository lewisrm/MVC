<?php

/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 04/11/2015
 * Time: 12:00 PM
 */
class FrontController
{
    private $request = "";
    private $controllerName = "";
    private $actionName = "";

    public function __construct()
    {
        if (isset($_GET["r"])) {
            $this->request = $_GET["r"];
        }
        if (isset($_POST["r"])) {
            $this->request = $_POST["r"];
        }
    }

    private function getUrl()
    {
        if (!empty($this->request)) {
            $url = explode("/", $this->request);
            $this->controllerName = $url[0] . "Controller";

            if (!isset($url[1])) {
                $this->actionName = "index";
            } else {
                $this->actionName = ucfirst($url[1]);
            }
        } else {
            $this->controllerName = "IndexController";
            $this->actionName = "index";
        }
    }

    /**
     * old method obsolete
     */
    public function run(Request $request)
    {
        $this->getUrl();
        $action = $this->actionName;
        $controller = new $this->controllerName();
        $controller->$action($request);
    }

    public function dispatch(Router $router, Dispatcher $dispatcher, Request $request)
    {
        $route = $router->router($request);
        $dispatcher->dispatch($route, $request);
    }
}

try {
    if (!file_exists('smvc/config/config.php')) {
        throw new Exception('Se requiere el archivo config.');
    }

    if (!file_exists('smvc/config/routes.php')) {
        throw new Exception('Se requiere el archivo routes.');
    }

    require_once 'smvc/config/config.php';

    /**
     * Debug mode
     */
    if ($config->get('display_errors')) {
        error_reporting(E_ALL);
        ini_set("display_errors", 1);
    }

    require_once 'core/autoloader.php';
    $autoload = new Autoloader();

    require_once 'config/routes.php';
    $request = new Request();

    /**
     * New implementation.
     * Comment this block of code when use the old way.
     */
    $router = new Router($routes);
    $dispatcher = new Dispatcher();
    $frontController = new FrontController();
    $frontController->dispatch($router, $dispatcher, $request);

    /**
     * old deprecated way, still available.
     * remove from block comment for use.
     * comment the new way when use this implementation
     *
     * $frontController->run($request);
     */
} catch (Exception $e) {
    echo $e->getMessage();
}