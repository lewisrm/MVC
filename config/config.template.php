<?php
/**
 * Created by PhpStorm.
 * User: levi
 * Date: 21/03/2017
 * Time: 09:50 AM
 */

require_once('smvc/core/Config.php');

$config = Config::singleton();

$config->set('dbhost', '');
$config->set('dbname', '');
$config->set('dbuser', '');
$config->set('dbpass', '');

$config->set('viewsFolder', 'smvc/views/');
$config->set('display_errors', false);

/**
 * /test or /sippep or /conpre
 */
$config->set('project_path', '');