<?php

/**
 * Created by PhpStorm.
 * User: levi
 * Date: 15/03/2017
 * Time: 09:12 AM
 */
class TestController extends ControllerBase
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        try {
            $model = new Index();
            $model->db->autocommit(false);
            #code
            $model->db->commit();
            $this->view->show('index.php', array());
        } catch (Exception $e) {
            $model->db->rollback();
            echo ($e->getMessage());
        }
    }
}