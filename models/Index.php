<?php

/**
 * Created by PhpStorm.
 * User: levi
 * Date: 15/03/2017
 * Time: 09:12 AM
 */

class Index extends ModelBase
{
    protected $table = 'cat_usuario';
    protected $fields = array('id_personal', 'login', 'id_tema', 'id_perfil', 'estatus', 'unidad_usuario', 'proyecto_usuario', 'no_modulo', 'iniciales');
    protected $protected_fields = array('id_usuario', 'password');
    protected $pk = 'id_usuario';


    public function __construct()
    {
        parent::__construct();
    }
}